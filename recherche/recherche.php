<?php

if (isset($_POST['prenom']) && !empty($_POST['prenom']) && !empty($_POST['nom']) && isset($_POST['nom'])){
    setcookie('nom', $_POST['nom'], time()+3600*24, '/', '', false, false);
    setcookie('prenom', $_POST['prenom'], time()+3600*24, '/', '', false, false);    
}
else if(isset($_GET['prenom']) && !empty($_GET['prenom']) && !empty($_GET['nom']) && isset($_GET['nom'])){
    setcookie('nom', $_GET['nom'], time()+3600*24, '/', '', false, false);
    setcookie('prenom', $_GET['prenom'], time()+3600*24, '/', '', false, false);
}
else if(isset($_POST['deconex']) && !empty($_POST['deconex'])){

    foreach($_COOKIE as $cookie_name => $cookie_value){

    // Commence par supprimer la valeur du cookie
    unset($_COOKIE[$cookie_name]);
       // Puis désactive le cookie en lui fixant 
       // une date d'expiration dans le passé
    setcookie('nom', '', time()-3600, '/', '', false, false);
    setcookie('prenom', '', time()-3600, '/', '', false, false);

    header("Location: ../index.php");
    }       
    
}
else{
    printf("");
}

//La partie du dessus va enregistrer un cookie avec les données de l'utilisateur

//Partie pour voir si le cookie existe ou pas

if(!empty($_COOKIE['prenom']) && !empty($_COOKIE['nom'])){
    $firstN =  $_COOKIE['prenom'];
    $lastN =  $_COOKIE['nom'];
}
else{
    $firstN =  $_GET['prenom'];
    $lastN =  $_GET['nom'];
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page </title>
</head>

<body>

    <h1>Page de recherche</h1>

    <aside>

        <h3>Vous êtes connectés</h3>

        <ul>
            <?php if(!empty($_COOKIE)):?>
            <li>Bienvenue <?php printf($_COOKIE["nom"]." ". $_COOKIE["prenom"])?></li>
            <?php else: ?>
            <li>Bienvenue <?php printf($_GET["nom"]." ". $_GET["prenom"])?></li>
            <?php endif; ?>
        </ul>

        <div>

            <form action=<?php echo "recherche.php?nom=$lastN&prenom=$firstN"?> method="post">
                <?php echo "<input type='hidden' name='cle' value='ok' />";?>

                <input type='submit' value='Afficher vos informations personnelles' />

            </form>

            <div class='info_perso'>

                <?php
                    if(isset($_POST['cle']) && !empty($_POST['cle'])){

                        require "../Manager/TraitementDataPerso.php";

                        $info_perso = recup_Info_Perso($_GET['nom'], $_GET['prenom']);

                         while ($res = $info_perso->fetch(PDO::FETCH_ASSOC)){

                
                            printf("<h2>Vos informations personnelles</h2>");
                            printf("<ul>");                

                                
                                printf('<li>Nom : '.htmlspecialchars(utf8_encode($res['nom'])).'</li>');
                                printf('<li>Prénom : '.htmlspecialchars(utf8_encode($res['prenom'])).'</li>');
                                printf('<li>Departement : '.htmlspecialchars(utf8_encode($res['depart'])).'</li>');
                                printf('<li>Mail : '.htmlspecialchars(utf8_encode($res['mail'])).'</li>');                    
                

                            printf("</ul>");

                        }


                    }

                ?>

            </div>
        </div>
    </aside>

    <h1>Recherchez des sportifs dans votre département</h1>


    <form action=<?php echo $_SERVER['SCRIPT_NAME'];?> method="post">
        <fieldset>

            <label for="sport">Choisir le sport :</label>

            <?php 
                    require "../bdd/requeteSql.php";

                    $sports = afficheSport();
            
                    printf("<select name='sport' id='sports'>");

                    while ($datas = $sports->fetch(PDO::FETCH_ASSOC)){

                        printf("<option value=".htmlspecialchars($datas['sport']).">".htmlspecialchars(utf8_encode($datas['sport']))."</option>");                    

                    }

                    printf("</select>");

                ?>

            <label for="niveau">Le niveau :</label>

            <select name="niveau" id="niveau">
                <option value="Supporter">Supporter</option>
                <option value="Débutant">Débutant</option>
                <option value="Confirmé">Confirmé</option>
                <option value="Professionnel">Professionnel</option>
            </select>

            <label for="depart">Choisir le numéro du département :</label>

            <?php                    

                    $dep = afficheDepart();
            
                    printf("<select name='depart' id='depart'>");

                    while ($donnees = $dep->fetch(PDO::FETCH_ASSOC)){

                        printf("<option value=".htmlspecialchars($donnees['depart']).">".htmlspecialchars(utf8_encode($donnees['depart']))."</option>");                    

                    }

                    printf("</select>");

                ?>

            <br>

            <input type='submit' value='Valider' />



        </fieldset>



    </form>

    <?php

            //Affiche les résultats du formulaire pour voir si une personne dans un département pratique le même sport


                    if(isset($_POST['depart']) && isset($_POST['sport'])):

                            if(!empty($_POST['depart']) && !empty($_POST['sport'])){

                                require_once '../Manager/TraitementRecherche.php';

                                    $result = traitRecherche2 ($_POST['sport'], $_POST['depart'], $_POST['niveau']);

                                    
                                    // var_dump(utf8_encode($donnees['nom']));
                                    // var_dump(utf8_encode($donnees['prenom']));
                                    // var_dump(utf8_encode($donnees['depart']));
                                    // var_dump(utf8_encode($donnees['niveau']));
                                    // var_dump(utf8_encode($donnees['sport']));

                                    ?>

    <div class='affiche'>


        <table class="table-striped">
            <tr>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Departement</th>
                <th>Niveau</th>
                <th>Sport</th>
                <th>Contact</th>

            </tr>

            <?php  
            

                        while ($donnees = $result->fetch(PDO::FETCH_ASSOC)){
                        
                        //var_dump($msgs);   
                        
                        printf("<tr>");
                        
                            foreach($donnees as $ligne){
                        printf('<td>'.htmlspecialchars(utf8_encode($ligne)).'</td>');
                                
                        }
                            printf('<td><button>Contactez-moi !</button></td>');
                        
                        printf("</tr>");
                        
                        }       

                                    
            ?>

        </table>

    </div>

    <?php    

                    }
                    else{
                        printf("");
                    }
                    else:
                            printf("");
                    endif;


            ?>

    <!-- Partie Rajouter un sport -->

    <h2>Si vous pratiquez un autre sport , rajoutez le !</h2>

    <?php

        if(isset($_COOKIE['nom']) && isset($_COOKIE['prenom'])){
            $nom_id = $_COOKIE['nom'];
            $prenom_id = $_COOKIE['prenom'];
        }
        else if(isset($_GET['nom']) && isset($_GET['prenom'])){
            $nom_id = $_GET['nom'];
            $prenom_id = $_GET['prenom'];
        }
        else{
            echo '';
        }

        //Message si le sport a été rajouté

        if(isset($_GET['msg']) && !empty($_GET['msg'])){
            printf('Votre sport a été rajouté.');
        }
        else if(isset($_GET['error']) && !empty($_GET['error'])){
            echo "Vous avez déjà rajouté ce sport.";
        }
        else{
            echo "";
        }
        


            ?>

    <form action="../bdd/requeteNewSport.php" method="post">

        <fieldset>

            <label for="sport">Choisir le sport</label>

            <?php 

            echo "<input type='hidden' name='nom' value='".$nom_id."' />";
            echo "<input type='hidden' name='prenom' value='".$prenom_id."' />";
                    
                    $sports = afficheSport();
                   
                    printf("<select name='sport' id='sports'>");

                    while ($datas = $sports->fetch(PDO::FETCH_ASSOC)){

                        printf("<option value=".htmlspecialchars($datas['id_sport']).">".htmlspecialchars(utf8_encode($datas['sport']))."</option>");                    

                    }

                    printf("</select>");

                    

                    ?>

            <p>

                <label for="level">Votre niveau :</label>

                <select name="level" id="niveau">
                    <option value="Supporter">Supporter</option>
                    <option value="Débutant">Débutant</option>
                    <option value="Confirmé">Confirmé</option>
                    <option value="Professionnel">Professionnel</option>
                </select>
            </p>

            <input type='submit' value='Valider' />

        </fieldset>
    </form>


    <!-- Partie déconnexion -->


    <div>
        <form action="<?php echo $_SERVER['SCRIPT_NAME'];?>" method="post">
            <input type="submit" name="deconex" value="Déconnexion" />
        </form>
    </div>



</body>

</html>