<?php

//Class qui traite les datas lors de l'inscription et qui les sanitize

class TraitementDatas {

    private $nom;
    private $prenom;
    private $departement;
    private $email;
    private $sport_id;
    private $niveau;
    
    public function __constructuct($nom , $prenom, $departement, $email, $sport_id, $niveau){

        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->departement = $departement;
        $this->email = $email;
        $this->sport_id = $sport_id;
        $this->niveau = $niveau;
    }

    public function getName (){
        return $this->nom;
    }
    public function getPrenom (){
        return $this->prenom;
    }
    public function getDepartement (){
        return $this->departement;
    }
    public function getemail (){
        return $this->email;
    }
    public function getSportId (){
        return $this->sport_id;
    }
    public function getNiveau (){
        return $this->niveau;
    }

    //Partie empty

    public function emptyNom($nom){

        if(!(empty($nom))){
            return $nom;
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");
        }
        
    }
    public function emptyPrenom($prenom){
        
        if(!(empty($prenom))){
            return $prenom;
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");
        }
        
    }
    public function emptyDepart($departement){
        
        if(!(empty($departement))){
            return $departement;
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");
        }
        
    }
    public function emptyEmail($email){

        if(!(empty($email))){
            return $email;
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");
        }
        
    }
    public function emptySportId($sport_id){

        if(!(empty($sport_id))){
            return $sport_id;
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");
        }
        
    }
    public function emptyNiveau($niveau){

        if(!(empty($niveau))){
            return $niveau;
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");
        }
        
    }

    //Partie sanitize string

    public function sanitizeNom ($nom){

        if(filter_var($nom, FILTER_SANITIZE_STRING)){
        $resultNom = filter_var($nom, FILTER_SANITIZE_STRING);
        return $resultNom;
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");    
        }
    }
    
    public function sanitizePrenom ($prenom){

        if(filter_var($prenom, FILTER_SANITIZE_STRING)){
        $resultPrenom = filter_var($prenom, FILTER_SANITIZE_STRING);
       
        return $resultPrenom;
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");
        }
    }
    public function sanitizeDepart ($depart){

        if(filter_var($depart, FILTER_SANITIZE_STRING)){
        $resultDepart = filter_var($depart, FILTER_SANITIZE_STRING);
        
        return $resultDepart;
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");            
        }
    }

    public function sanitizeEmail ($email){

        if(filter_var($email, FILTER_SANITIZE_EMAIL)){
        $resultEmail = filter_var($email, FILTER_SANITIZE_EMAIL);
        return $resultEmail;
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");
        }
    }

    public function sanitizeSportId ($sport_id){

        if(filter_var($sport_id, FILTER_SANITIZE_STRING)){
        $resultId = filter_var($sport_id, FILTER_SANITIZE_STRING);
        return $resultId;
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");
        }
    }

    public function sanitizeNiveau ($niveau){

        if($niveau === "Débutant" || $niveau === "Supporter" || $niveau === "Confirmé" || $niveau === "Professionnel"){
        $resultNiveau = $niveau;
        return $resultNiveau;
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");            
        }
    }

    //Partie trois

    public function sanitizeNomChars ($nom){

        if(filter_var($nom, FILTER_SANITIZE_FULL_SPECIAL_CHARS)){
        $resultNom = filter_var($nom, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        
        return $resultNom;
        }
        else{
           header("Location: ../ajout/ajout.php?error=data");
        }
    }
    
    public function sanitizePrenomChars ($prenom){
        
        if(filter_var($prenom, FILTER_SANITIZE_FULL_SPECIAL_CHARS)){
            $resultPrenom = filter_var($prenom, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            
            return $resultPrenom;
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");           
        }
    }

    public function sanitizeDepartChars ($depart){

        //var_dump($depart);
                
        if(preg_match('/^(([\d]{2} )|(2[abAB] ))*(([\d]{2})|(2[abAB]))$/', $depart)){
            
            $resultDepart = $depart;            
            return $resultDepart;
            
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");            
        }
    }

    public function sanitizeEmailChars ($email){
        if(filter_var($email, FILTER_SANITIZE_FULL_SPECIAL_CHARS)){
            $resultEmail = filter_var($email, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            return $resultEmail;
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");            
        }       
    }
    
    public function sanitizeCharSportId ($id){
        if(filter_var($id, FILTER_SANITIZE_FULL_SPECIAL_CHARS)){
            $resultId = filter_var($id, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            return $resultId;
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");
            echo '17';            
        }       
    }

    public function sanitizeCharNiveau ($niveau){
        if(filter_var($niveau, FILTER_SANITIZE_FULL_SPECIAL_CHARS)){

            if($niveau === "Supporter" || $niveau === "Débutant" || $niveau === "Confirmé" || $niveau === "Professionnel"){
            $resultNiveau = $niveau;
            return $resultNiveau;
            }
            else{

                header("Location: ../ajout/ajout.php?error=data");                             
            }
        }
        else{
            header("Location: ../ajout/ajout.php?error=data");
            
        }       
    }
}

//Début des tests 



if($_POST['departement'] === "99" || $_POST['departement'] === "98" || $_POST['departement'] === "97" || $_POST['departement'] === "00" || $_POST['departement'] === "96"){
        header("Location: ../ajout/ajout.php?error=data");        
}



if(isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['departement']) && isset($_POST['email']) &&
isset($_POST['sport']) && isset($_POST['niveau'])){

    

        $newUser = new TraitementDatas($_POST['nom'], $_POST['prenom'], $_POST['departement'], $_POST['email'], $_POST['sport'], $_POST['niveau']);

    
        $nomTest = $newUser->emptyNom($_POST['nom']);
        $prenomTest = $newUser->emptyPrenom($_POST['prenom']);
        $departTest = $newUser->emptyDepart($_POST['departement']);
        $mailTest = $newUser->emptyEmail($_POST['email']);
        $sportTest = $newUser->emptySportId($_POST['sport']);
        $niveauTest = $newUser->emptyNiveau($_POST['niveau']);

        $nomSani = $newUser->sanitizeNom($nomTest);
        $prenomSani = $newUser->sanitizePrenom($prenomTest);
        $departSani = $newUser->sanitizeDepart($departTest);
        $mailSani = $newUser->sanitizeEmail($mailTest);
        $sportSani = $newUser->sanitizeSportId($sportTest);
        $niveauSani = $newUser->sanitizeNiveau($niveauTest);

        $finalNom = $newUser->sanitizeNomChars($nomSani);
        $finalPrenom = $newUser->sanitizePrenomChars($prenomSani);
        $finalDepart = $newUser->sanitizeDepartChars($departSani);
        $finalMail = $newUser->sanitizeEmailChars($mailSani);
        $finalSport = $newUser->sanitizeCharSportId($sportSani);
        $finalNiveau = $newUser->sanitizeCharNiveau($niveauSani);

                
        //On insère d'abord dans la table personne

        $idSport = intval($finalSport);         

        //Verification si l'email est bien présent en bdd

        require_once("../Manager/TraitIdentif.php");

        $result = verifMail($finalMail);

        if($result->rowCount() > 0) //si il y a des données
        {
            header("Location: ../ajout/ajout.php?email=email");
        }
        else{

        require_once('../bdd/requeteSql.php');

        insertPersonne($finalNom, $finalPrenom, $finalDepart, $finalMail);
                
        
        //récuperer l'id de la derniere personne rentrée dans la table
        
        require_once('../bdd/requeteSqlSport.php');

                
        
        $id = recupId();
        
        $person = $id->fetch(PDO::FETCH_NUM);

        $id_person = $person[0];

        $id_personne = intval($id_person);

        
        insertPratique($id_personne, $idSport, $finalNiveau);
        
        header("Location: ../recherche/recherche.php?nom=$finalNom&prenom=$finalPrenom");

        }
    
    
        
    
}
else{
    $e = "Erreur dans les données à traiter 33 !";
    printf($e);
}
