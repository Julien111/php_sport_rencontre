<?php

const RECUP_INFOS ="SELECT P.nom, P.prenom, P.depart, R.niveau, S.sport FROM pratique AS R INNER JOIN personne AS P ON P.id_personne = R.id_personne INNER JOIN sport AS S ON S.id_sport = R.id_sport WHERE S.sport = :sport AND P.depart = :depart AND R.niveau = :niveau";

function traitRecherche2 ($sport, $depart, $niveau) {

    $depart = htmlspecialchars($depart);

    $sport = htmlspecialchars($sport);

    $niveau = htmlspecialchars($niveau);
    
    $sportUn = utf8_decode($sport);
    $departUn = utf8_decode($depart);
    $niveauUn = utf8_decode($niveau);

    try {
        
    require("../bdd/bdd.php");
    
    $datas = $MyDB->prepare(RECUP_INFOS);
    $datas->bindParam(":sport", $sportUn, PDO::PARAM_STR);
    $datas->bindParam(":depart", $departUn, PDO::PARAM_STR);
    $datas->bindParam(":niveau", $niveauUn, PDO::PARAM_STR);
    $datas->execute();

    if($datas->rowCount() > 0) //si il y a des données
    {
        return $datas;
    }
    else{
        return "";
    }

    $datas->closeCursor();


    } catch (Exception $e) {
        echo 'Erreur reçue : ',  $e->getMessage(), "\n";
    }


}