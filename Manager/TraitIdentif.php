<?php

//Requete pour vérifier l'email

const VERIF_EMAIL = "SELECT nom, prenom, mail FROM personne WHERE mail = :email";

//Partie verification email page connection


function verifMail($email){

    try {

        require "../bdd/bdd.php";

        $mail = $MyDB->prepare(VERIF_EMAIL);
        $mail->bindParam(":email", $email, PDO::PARAM_STR);
        $mail->execute();
        
        return $mail;
        $mail->closeCursor();

    } catch (Exception $e) {
    echo 'Erreur reçue : ',  $e->getMessage(), "\n";
    }
}

// Class qui traite le login de connection par l'email


class TraitIndentif{

    private $email;
    
    public function __constructuct($email){
        $this->email = $email;
    }

    public function getEmail (){
        return $this->email;
    }

    public function emptyEmail($email){

        if(!(empty($email))){
            return $email;
        }
        else{
            header("Location: ../index.php?error=data");
        }
        
    }

    public function sanitizeEmail ($email){

        if(filter_var($email, FILTER_SANITIZE_EMAIL)){
        $resultEmail = filter_var($email, FILTER_SANITIZE_EMAIL);
        return $resultEmail;
        }
        else{
            header("Location: ../index.php?error=data");
        }
    }

    public function sanitizeEmailChars ($email){
        if(filter_var($email, FILTER_SANITIZE_FULL_SPECIAL_CHARS)){
            $resultEmail = filter_var($email, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            return $resultEmail;
        }
        else{
            header("Location: ../index.php?error=data");            
        }       
    }
}

if(isset($_POST['email'])){

    $email = $_POST['email'];

    $user = new TraitIndentif($email);

    $mailTest = $user->emptyEmail($_POST['email']);
    $mailSani = $user->sanitizeEmail($mailTest);
    $mailFinal = $user->sanitizeEmailChars($mailSani);

    $verif = verifMail($mailFinal);

    if($verif->rowCount() > 0){
        
        while ($data = $verif->fetch(PDO::FETCH_ASSOC)){

            $name = $data['nom'];
            $firstName = $data['prenom'];

            header("Location: ../recherche/recherche.php?nom=$name&prenom=$firstName");
        }
    }
    else{
        header("Location: ../ajout/ajout.php?errlog=login");
    }

}