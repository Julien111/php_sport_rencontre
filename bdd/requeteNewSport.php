<?php

// var_dump($_POST['prenom']);
// var_dump($_POST['nom']);
// var_dump($_POST['level']);
// var_dump($_POST['sport']);

const RECUP_SPORT_ID = "SELECT id_personne FROM personne WHERE nom = :nom AND prenom = :prenom";

const VERIF_SPORT = "SELECT id_sport FROM pratique WHERE id_personne = :id_personne AND id_sport = :id_sport";

const INSERT_VALUES_PRA = "INSERT INTO pratique (id_personne, id_sport, niveau) VALUES (:id_personne, :id_sport, :niveau)";

//class pour traiter les infos nom et prenom

class TraitPersonneDatas{

    private $nom;

    private $prenom;

        
    public function __constructuct($nom, $prenom){
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public function getNom (){
        return $this->nom;
    }

    public function getPrenom (){
        return $this->prenom;
    }

    public function emptyNom($nom){

        if(!(empty($nom))){
            return $nom;
        }
        else{
            header("Location: ../recherche/recherche.php?error=data21");
        }
        
    }

    public function emptyPrenom($prenom){

        if(!(empty($prenom))){
            return $prenom;
        }
        else{
            header("Location: ../recherche/recherche.php?error=data22");
        }
        
    }

    public function sanitizeNom ($nom){

        if(filter_var($nom, FILTER_SANITIZE_STRING)){
        $resultNom = filter_var($nom, FILTER_SANITIZE_STRING);
        return $resultNom;
        }
        else{
            header("Location: ../recherche/recherche.php?error=data23");
        }
    }

    public function sanitizePrenom ($prenom){

        if(filter_var($prenom, FILTER_SANITIZE_STRING)){
        $resultPrenom = filter_var($prenom, FILTER_SANITIZE_STRING);
        return $resultPrenom;
        }
        else{
            header("Location: ../recherche/recherche.php?error=data24");
        }
    }
    
}


//classe pour traiter les données sport et niveau


class TraitDatasSport {

    private $level;

    private $sport;


    public function __constructuct($level, $sport){
        $this->level = $level;
        $this->sport = $sport;
    }

    public function getLevel (){
        return $this->level;
    }

    public function getSport (){
        return $this->sport;
    }

    
    public function emptyLevel($level){

        if(!(empty($level))){
            return $level;
        }
        else{
            header("Location: ../recherche/recherche.php?error=data27");
        }
        
    }

    public function emptySport($sport){

        if(!(empty($sport))){
            return $sport;
        }
        else{
            header("Location: ../recherche/recherche.php?error=data28");
        }
        
    }

    public function sanitizeLevel ($level){

        if(filter_var($level, FILTER_SANITIZE_STRING)){
        $resultLevel = filter_var($level, FILTER_SANITIZE_STRING);
        return $resultLevel;
        }
        else{
            header("Location: ../recherche/recherche.php?error=data29");
        }
    }

    public function sanitizeSport ($sport){

        if(filter_var($sport, FILTER_SANITIZE_STRING)){
        $resultSport = filter_var($sport, FILTER_SANITIZE_STRING);
        return $resultSport;
        }
        else{
            header("Location: ../recherche/recherche.php?error=data30");
        }
    }

    
}

function recupIdPerson($nom, $prenom){

    try {

        require "bdd.php";

        $id_person = $MyDB->prepare(RECUP_SPORT_ID);
        $id_person->bindParam(":nom", $nom, PDO::PARAM_STR);
        $id_person->bindParam(":prenom", $prenom, PDO::PARAM_STR);
        $id_person->execute();
        
        return $id_person;
        $id_person->closeCursor();

    } catch (Exception $e) {
    echo 'Erreur reçue : ',  $e->getMessage(), "\n";
    }


}

//Fonction pour injecter des données pour

function injecteDonnee ($id_person, $id_sport, $niveau) {
        
    try {

    require "bdd.php";

    $data = $MyDB->prepare(INSERT_VALUES_PRA);
    $data->bindParam(':id_personne', $id_person, PDO::PARAM_INT);
    $data->bindParam(':id_sport', $id_sport,PDO::PARAM_INT);
    $data->bindParam(':niveau', $niveau, PDO::PARAM_STR);
    $data->execute();
        
    } catch (Exception $e) {
        echo 'Erreur reçue : ',  $e->getMessage(), "\n";
    }
}


// var_dump

function verifIdSport($id_personne, $id_sport){

try {

        require "../bdd/bdd.php";

        $id = $MyDB->prepare(VERIF_SPORT);
        $id->bindParam(":id_personne", $id_personne, PDO::PARAM_INT);
        $id->bindParam(":id_sport", $id_sport, PDO::PARAM_INT);
        $id->execute();
        
        return $id;
        $id->closeCursor();

    } catch (Exception $e) {
    echo 'Erreur reçue : ',  $e->getMessage(), "\n";
    }
}

if(isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['level']) && isset($_POST['sport'])){

//Récup l'id de la personne  

    $recup_Id = new TraitPersonneDatas($_POST['nom'], $_POST['prenom']);
    $nameUn = $recup_Id->emptyNom($_POST['nom']);
    $prenomUn = $recup_Id->emptyPrenom($_POST['prenom']);

    $nameSani = $recup_Id->sanitizeNom($nameUn);
    $prenomSani = $recup_Id->sanitizePrenom($prenomUn);

    $nameFinal = htmlspecialchars(utf8_decode($nameSani));
    $prenomFinal = htmlspecialchars(utf8_decode($prenomSani));

    // var_dump($nameFinal);
    // var_dump($prenomFinal);

    //Traitement des données sport et leveling

    $traitementDatasSport = new TraitDatasSport($_POST['level'], $_POST['sport']);   
    
    $levelUn = $traitementDatasSport->emptyLevel($_POST['level']);
    $sportUn = $traitementDatasSport->emptySport($_POST['sport']);

    $levelSani = $traitementDatasSport->sanitizeLevel($levelUn);
    $sportSani = $traitementDatasSport->sanitizeSport($sportUn);

     //var_dump($levelSani);

    $levelFinal = utf8_decode($levelSani);
    $sportFinal = htmlspecialchars(utf8_encode($sportSani));

    //Fin des traitements
    
    $resultId = recupIdPerson($nameFinal, $prenomFinal);

    $datas = $resultId->fetch(PDO::FETCH_ASSOC);     

    //On a récupéré l'id de la personne et on va insérer dans la table pratique

    $id_personne = intval($datas['id_personne']);
    $id_sport = intval($sportFinal);


    $verif = verifIdSport($id_personne, $id_sport);

    if($verif->rowCount() > 0){
        header("Location: ../recherche/recherche.php?error=err");
    }
    else{
    // var_dump($id_personne);
    // var_dump( $id_sport);
    //var_dump($niveau);

    //On injecte les datas

    injecteDonnee($id_personne, $id_sport, $levelFinal);

    header("Location: ../recherche/recherche.php?msg=ok");
    }


}