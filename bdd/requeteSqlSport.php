<?php

const RECUP_ID = "SELECT MAX(id_personne) AS max_id FROM personne";

const INSERT_PRATIQUE = "INSERT INTO pratique (id_personne, id_sport, niveau) VALUES (:id_personne, :id, :niveau)";

const INSERT_SPORT = "INSERT INTO sport (sport, description) VALUES (:sport, :descrip)";

const VERIF_SPORT = "SELECT sport FROM sport WHERE sport = :sport";

function recupId (){

    try {
        
    require("bdd.php");
    
    $id_rec = $MyDB->prepare(RECUP_ID);
    $id_rec->execute();

    if($id_rec->rowCount() > 0) //si il y a des données
    {
        return $id_rec;
    }
    else{
        return "";
    }

    $id_rec->closeCursor();


    } catch (Exception $e) {
        echo 'Erreur reçue : ',  $e->getMessage(), "\n";
    }
}

function insertPratique ($id_personne, $id, $niveau){

    try {

        $niveauUn = utf8_decode($niveau);
        
    require("bdd.php");
    
    $donnees = $MyDB->prepare(INSERT_PRATIQUE);
    $donnees->bindParam(':id_personne', $id_personne, PDO::PARAM_INT);
    $donnees->bindParam(':id', $id, PDO::PARAM_INT);
    $donnees->bindParam(':niveau', $niveauUn, PDO::PARAM_STR);
    $donnees->execute();

    $donnees->closeCursor();


    } catch (Exception $e) {
        echo 'Erreur reçue : ',  $e->getMessage(), "\n";
    }
}

//Ajouter un sport

function verifSport ($sport){

    try {

        require "../bdd/bdd.php";
       
        $sports = $MyDB->prepare(VERIF_SPORT);
        $sports->bindParam(":sport", $sport, PDO::PARAM_STR);
        $sports->execute();
        
        return $sports;
        $sports->closeCursor();

    } catch (Exception $e) {
    echo 'Erreur reçue : ',  $e->getMessage(), "\n";
    }
}

function insertSport ($sport, $description){

    try {

        $sportUn = utf8_decode($sport);
        $descriUn = utf8_decode($description);
        
    require("bdd.php");
    
    $donnees = $MyDB->prepare(INSERT_SPORT);
    $donnees->bindParam(':sport', $sportUn, PDO::PARAM_STR);
    $donnees->bindParam(':descrip', $descriUn, PDO::PARAM_STR);
    $donnees->execute();

    $donnees->closeCursor();


    } catch (Exception $e) {
        echo 'Erreur reçue : ',  $e->getMessage(), "\n";
    }
}

if(isset($_POST['description']) && isset($_POST['sport'])){

    if(!empty($_POST['description']) && !empty($_POST['sport'])){

        if(htmlspecialchars($_POST['description']) && htmlspecialchars($_POST['sport'])){

            $sportUn = filter_var($_POST['sport'], FILTER_SANITIZE_STRING);
            $description = filter_var($_POST['description'], FILTER_SANITIZE_STRING);

            $verif_sport = verifSport($sportUn);
            if($verif_sport->rowCount() > 0){

                header("Location: ../ajout/ajout.php?sport=data");
            }
            else{                
                insertSport ($sportUn, $description);
                header("Location: ../ajout/ajout.php?newsport=data");
            }
        }
    }
}