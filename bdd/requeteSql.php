<?php

//Les requêtes

const SPORTS_ALL = "SELECT sport, description FROM sport";

const ALL_DEPART = "SELECT DISTINCT depart from personne";

const NIVEAU = "SELECT niveau FROM pratique";

const ALL_NAME_SPORT= "SELECT id_sport, sport FROM sport";

const INSERT_TABLE_PERSON = "INSERT INTO personne (nom, prenom, depart, mail) VALUES (:nom, :prenom, :depart, :mail)";

//Les fonctions


function getSport () {

    
     try {

        require "bdd.php";

        $sports = $MyDB->prepare(SPORTS_ALL);
        $sports->execute();
        if($sports->rowCount() > 0) //si il y a des données
        {
            return $sports;
        }
        else{
            return "";
        }

        $sports->closeCursor();

    } catch (Exception $e) {
    echo 'Erreur reçue : ',  $e->getMessage(), "\n";
    }   
}

function insertPersonne($nom, $prenom, $depart, $mail){

   try {

    require "bdd.php";

    $data = $MyDB->prepare(INSERT_TABLE_PERSON);
    $data->bindParam(':nom', $nom, PDO::PARAM_STR);
    $data->bindParam(':prenom', $prenom,PDO::PARAM_STR);
    $data->bindParam(':depart', $depart, PDO::PARAM_STR);
    $data->bindParam(':mail', $mail, PDO::PARAM_STR);
    $data->execute();
        
   
    } catch (Exception $e) {
        echo 'Erreur reçue : ',  $e->getMessage(), "\n";
    }
}

function afficheSport (){

    try {

        require "bdd.php";

        $sportAll = $MyDB->prepare(ALL_NAME_SPORT);
        $sportAll->execute();
        if($sportAll->rowCount() > 0) //si il y a des données
        {
            return $sportAll;
        }
        else{
            return "";
        }

        $sportsAll->closeCursor();

    } catch (Exception $e) {
    echo 'Erreur reçue : ',  $e->getMessage(), "\n";
    }   
}

function getNiveau (){

    try {

        require "bdd.php";

        $niveaux = $MyDB->prepare(NIVEAU);
        $niveaux->execute();
        if($niveaux->rowCount() > 0) //si il y a des données
        {
            return $niveaux;
        }
        else{
            return "";
        }

        $niveaux->closeCursor();

    } catch (Exception $e) {
    echo 'Erreur reçue : ',  $e->getMessage(), "\n";
    }   
}

//Afficher les département dans la page recherche


function afficheDepart (){

    try {

        require "bdd.php";

        $departAll = $MyDB->prepare(ALL_DEPART);
        $departAll->execute();
        if($departAll->rowCount() > 0) //si il y a des données
        {
            return $departAll;
        }
        else{
            return "";
        }

        $departAll->closeCursor();

    } catch (Exception $e) {
    echo 'Erreur reçue : ',  $e->getMessage(), "\n";
    }   
}