<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inscription</title>
</head>

<body>

    <div>
        <h2>Page d'inscription</h2>
    </div>

    <?php

        if(isset($_GET['error']) && !empty($_GET['error'])){
            printf("<p><strong>Erreur dans la saisie de vos données !</strong></p>");
        }else if(isset($_GET['msg']) && !empty($_GET['msg'])){
            printf("<p><strong>Les données ont bien été envoyées !</strong></p>");
        }
        else if(isset($_GET['email']) && !empty($_GET['email'])){
            printf("<p><strong>Email déjà enregistré !</strong></p>");
        }
        else if(isset($_GET['sport']) && !empty($_GET['sport'])){
            printf("<p><strong>Sport déjà enregistré !</strong></p>");
        }
        else if(isset($_GET['newsport']) && !empty($_GET['newsport'])){
            printf("<p><strong>Le nouveau sport a bien été enregistré !</strong></p>");
        }
        else if(isset($_GET['errlog']) && !empty($_GET['errlog'])){
            printf("<p><strong>Vous n'avez pas de compte, inscrivez-vous si vous souhaitez bénéficier du service.</strong></p>");
        }
        else{
            echo "";
        }

    ?>

    <div class='formulaire'>

        <form action="./../Manager/Traitement.php" method='post'>
            <p>
            <label for='nom'>Nom</label>
            <input type='text' id='nom' name='nom' required />
            </p>

            <p>
            <label for='prenom'>Prenom</label>
            <input type='text' id='prenom' name='prenom' required />
            </p>
            <p>
            <label for='email'>Email</label>
            <input type='email' id='email' name='email' required />
            </p>

            <p>
            <label for='departement'>Le numéro de votre Département : </label>
            <input type='text' maxlength='2' name="departement" required />
            </p>

             <fieldset>

                <label for="sport">Choisir votre sport préféré:</label>

                <?php 
                    require "../bdd/requeteSql.php";

                    $sports = afficheSport();
                   
                    printf("<select name='sport' id='sports'>");

                    while ($datas = $sports->fetch(PDO::FETCH_ASSOC)){

                        printf("<option value=".htmlspecialchars($datas['id_sport']).">".htmlspecialchars(utf8_encode($datas['sport']))."</option>");                    

                    }

                    printf("</select>");

                ?>                   

                    <label for="niveau">Votre niveau :</label>

                    <select name="niveau" id="niveau">
                        <option value="Supporter">Supporter</option>
                        <option value="Débutant">Débutant</option>
                        <option value="Confirmé">Confirmé</option>
                        <option value="Professionnel">Professionnel</option>                        
                    </select>

                    
            </fieldset> 

            <br>

            <input type='submit' value='Valider' />
        </form>

    </div>

    <div class='ajout_sport'>

                <h2>Ajouter un sport</h2>

                <p>Si votre sport n'est pas dans la liste, ajoutez le pour pouvoir valider votre inscription
                </p>

                <form action="../bdd/requeteSqlSport.php" method="post">
                    <p>
                    <label for='sport'>Nom du sport </label>
                    <input type='text' id='sport' name='sport' required />
                    </p>

                    <p>
                    <label for='description'>Description </label>
                    <textarea type='text' id='description' name='description' required></textarea>
                    </p>

                    <input type='submit' value='Valider' />
                </form>
    </div>

    <div>
        <p><a href='./../index.php'>Retour à l'accueil</a></p>
    </div>


</body>

</html>